import { ConfigImpl } from './config';
export declare function validateGhostURL(uri: string): boolean | string;
export declare function isGitURL(uri: string): boolean | string;
export declare function checkIfDeployable(config: ConfigImpl): Promise<void>;
export declare function validateURL(url: string): Promise<void>;
//# sourceMappingURL=validate.d.ts.map