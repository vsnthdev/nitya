#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var caporal_1 = __importDefault(require("caporal"));
var update_notifier_1 = __importDefault(require("update-notifier"));
var errorHandler_1 = __importDefault(require("./errorHandler"));
var config_1 = __importStar(require("./config"));
var logger_1 = __importDefault(require("./logger"));
var root_1 = __importDefault(require("./cmd/root"));
var build_1 = __importDefault(require("./cmd/build"));
var serve_1 = __importDefault(require("./cmd/serve"));
var deploy_1 = __importDefault(require("./cmd/deploy"));
var packageInfo = require('../package.json');
update_notifier_1.default({ pkg: packageInfo, updateCheckInterval: 0 }).notify();
errorHandler_1.default(config_1.default()).then(function (data) {
    if (data.error) {
        logger_1.default.error(data.error);
    }
    else {
        var config_2 = data.data;
        caporal_1.default
            .version("v" + packageInfo.version)
            .description(packageInfo.description)
            .option('--init', 'Creates a new configuration file in the current directory', caporal_1.default.BOOLEAN, false, false)
            .action(function (args, options) {
            root_1.default(options, config_2);
        });
        caporal_1.default
            .command('build', 'Convert Ghost site into static website')
            .argument('[url]', 'The Ghost site URL from which static website should be generated', '', config_2.uri)
            .option('--minify <value>', 'Minify the static site', caporal_1.default.BOOLEAN, config_2.minify, false)
            .option('--force', 'Overwrite the existing public directory. If exists', caporal_1.default.BOOLEAN, false, false)
            .action(function (args, options) {
            build_1.default(args, options, config_2);
        });
        caporal_1.default
            .command('serve', 'Preview the built static website')
            .option('--port [number]', 'Port number to bind', caporal_1.default.INTEGER, config_2.server.port, false)
            .option('--host [number]', 'Host to listen for incoming requests', caporal_1.default.STRING, config_2.server.host, false)
            .option('--open', 'Open browser after starting the server', caporal_1.default.BOOLEAN, false, false)
            .action(function (args, options) {
            serve_1.default(args, options);
        });
        caporal_1.default
            .command('deploy', 'Deploy the static site to a cloud service')
            .option('--domain [domain]', 'Creates a CNAME file while deploying for custom domains', caporal_1.default.STRING, config_1.getDomain({}, config_2), false)
            .action(function (args, options) {
            deploy_1.default(options, config_2);
        });
        caporal_1.default.parse(process.argv);
    }
});
//# sourceMappingURL=nitya.js.map