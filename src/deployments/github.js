"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var gh_pages_1 = __importDefault(require("gh-pages"));
var validate_1 = require("../validate");
var logger_1 = __importDefault(require("../logger"));
function validateGitHubDeployment(deploySettings) {
    return __awaiter(this, void 0, void 0, function () {
        var repositoryURL;
        return __generator(this, function (_a) {
            repositoryURL = deploySettings.repository == '' ||
                typeof deploySettings.repository == 'undefined'
                ? 'The deployment provider "gh-pages" requires a repository field'
                : validate_1.isGitURL(deploySettings.repository);
            if (typeof repositoryURL == 'string') {
                logger_1.default.error(repositoryURL, 7);
            }
            return [2];
        });
    });
}
exports.validateGitHubDeployment = validateGitHubDeployment;
function github(publicDirectory, options, config, spinner) {
    return new Promise(function (resolve) {
        var deploySettings = config.deploy;
        var githubDeployOptions = deploySettings;
        spinner.text = "Deploying the static site to " + deploySettings.provider;
        var ghpagesOptions = {
            branch: 'gh-pages',
            repo: githubDeployOptions.repository,
            message: githubDeployOptions.message
                ? githubDeployOptions.message
                : '📗 Deployed static site by nitya',
            history: false,
        };
        gh_pages_1.default.publish(publicDirectory, ghpagesOptions, function (err) {
            if (err) {
                throw err;
            }
            else {
                resolve();
            }
        });
    });
}
exports.default = github;
//# sourceMappingURL=github.js.map