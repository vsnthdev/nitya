import ora from 'ora';
import { DeployProviderConfigImpl } from '../config';
import { ConfigImpl } from '../config';
export interface GitHubConfigImpl extends DeployProviderConfigImpl {
    repository: string;
    message?: string;
}
export declare function validateGitHubDeployment(deploySettings: GitHubConfigImpl): Promise<void>;
export default function github(publicDirectory: string, options: any, config: ConfigImpl, spinner: ora.Ora): Promise<void>;
//# sourceMappingURL=github.d.ts.map