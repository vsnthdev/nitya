"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function errorHandler(target) {
    return new Promise(function (resolve) {
        target
            .then(function (anyData) {
            var returnableResponse = {
                error: undefined,
                data: anyData,
            };
            resolve(returnableResponse);
        })
            .catch(function (anError) {
            var returnableResponse = {
                error: anError,
                data: undefined,
            };
            resolve(returnableResponse);
        });
    });
}
exports.default = errorHandler;
//# sourceMappingURL=errorHandler.js.map