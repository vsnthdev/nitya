"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var inquirer_1 = __importDefault(require("inquirer"));
var execa_1 = __importDefault(require("execa"));
var moment_1 = __importDefault(require("moment"));
var del_1 = __importDefault(require("del"));
var mkdirp_1 = __importDefault(require("mkdirp"));
var chalk_1 = __importDefault(require("chalk"));
var ora_1 = __importDefault(require("ora"));
var globby_1 = __importDefault(require("globby"));
var html_minifier_1 = __importDefault(require("html-minifier"));
var csso_1 = __importDefault(require("csso"));
var replace_in_file_1 = __importDefault(require("replace-in-file"));
var errorHandler_1 = __importDefault(require("../errorHandler"));
var logger_1 = __importDefault(require("../logger"));
var validate_1 = require("../validate");
function fetchStatics(url, publicDirectory, spinner) {
    return __awaiter(this, void 0, void 0, function () {
        var date, domain, wget;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    mkdirp_1.default.sync(publicDirectory);
                    process.chdir(publicDirectory);
                    date = moment_1.default().format('YYYY-MM-DD');
                    domain = new URL(url).host;
                    wget = execa_1.default('/usr/bin/wget', [
                        '--recursive',
                        '--page-requisites',
                        '--html-extension',
                        '--convert-links',
                        '--restrict-file-names=unix',
                        '--no-parent=on',
                        '--no-host-directories',
                        '--no-cache',
                        '--no-dns-cache',
                        "--domains=" + domain,
                        url,
                    ]);
                    wget.stderr.setEncoding('utf8');
                    wget.stderr.on('data', function (data) {
                        if (data.startsWith(date)) {
                            var filename = data.split('‘')[1].split('’')[0];
                            spinner.text = "Downloading " + chalk_1.default.gray(filename.split('?')[0]);
                        }
                    });
                    return [4, wget];
                case 1:
                    _a.sent();
                    return [2];
            }
        });
    });
}
exports.fetchStatics = fetchStatics;
function handlePublicDirectory(publicDirectory, options, config) {
    return __awaiter(this, void 0, void 0, function () {
        var exists, overwrite;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, errorHandler_1.default(fs_1.default.promises.access(publicDirectory))];
                case 1:
                    exists = _a.sent();
                    if (!(typeof exists.error !== 'undefined')) return [3, 2];
                    return [2];
                case 2:
                    if (!(typeof config.file == 'string' && config.file !== '')) return [3, 6];
                    if (!(options.force == true)) return [3, 4];
                    return [4, del_1.default(publicDirectory)];
                case 3:
                    _a.sent();
                    return [2];
                case 4:
                    logger_1.default.error('A public directory already exists', 4);
                    _a.label = 5;
                case 5: return [3, 12];
                case 6:
                    if (!(options.force == true)) return [3, 8];
                    return [4, del_1.default(publicDirectory)];
                case 7:
                    _a.sent();
                    return [2];
                case 8: return [4, inquirer_1.default.prompt({
                        type: 'confirm',
                        message: 'There is already a public directory. Would you like to overwrite?',
                        name: 'overwrite',
                        default: false,
                    })];
                case 9:
                    overwrite = _a.sent();
                    if (!(overwrite.overwrite == false)) return [3, 10];
                    process.exit(0);
                    return [3, 12];
                case 10: return [4, del_1.default(publicDirectory)];
                case 11:
                    _a.sent();
                    return [2];
                case 12: return [2];
            }
        });
    });
}
exports.handlePublicDirectory = handlePublicDirectory;
function fixWgetLinks(publicDirectory) {
    return __awaiter(this, void 0, void 0, function () {
        function fixLinks(htmlFiles) {
            return __awaiter(this, void 0, void 0, function () {
                var replaceOptions;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            replaceOptions = {
                                files: htmlFiles,
                                from: [/jpgpg/g, /pngng/g, /jpgg/g, /pngg/g, /jpgjpg/g, /pngpng/g],
                                to: ['jpg', 'png', 'jpg', 'png', 'jpg', 'png'],
                            };
                            return [4, replace_in_file_1.default(replaceOptions)];
                        case 1:
                            _a.sent();
                            return [2];
                    }
                });
            });
        }
        var matches;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, globby_1.default(publicDirectory + "/**/*.+(html|htm)")];
                case 1:
                    matches = _a.sent();
                    return [4, fixLinks(matches)];
                case 2:
                    _a.sent();
                    return [2];
            }
        });
    });
}
exports.fixWgetLinks = fixWgetLinks;
function minifyStaticSite(minify, publicDirectory, spinner) {
    return __awaiter(this, void 0, void 0, function () {
        function minifyHTML(htmlFile) {
            return __awaiter(this, void 0, void 0, function () {
                var htmlString, minifyOptions, minifiedHTML;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4, fs_1.default.promises.readFile(htmlFile, {
                                encoding: 'utf8',
                            })];
                        case 1:
                            htmlString = _a.sent();
                            minifyOptions = {
                                collapseBooleanAttributes: true,
                                collapseWhitespace: true,
                                conservativeCollapse: true,
                                continueOnParseError: true,
                                minifyCSS: true,
                                minifyJS: true,
                                minifyURLs: true,
                                removeAttributeQuotes: true,
                                removeComments: true,
                                removeEmptyAttributes: true,
                                sortAttributes: true,
                                sortClassName: true,
                            };
                            minifiedHTML = html_minifier_1.default.minify(htmlString, minifyOptions);
                            return [4, fs_1.default.promises.writeFile(htmlFile, minifiedHTML, {
                                    encoding: 'utf8',
                                })];
                        case 2:
                            _a.sent();
                            return [2];
                    }
                });
            });
        }
        function minifyCSS(cssFile) {
            return __awaiter(this, void 0, void 0, function () {
                var cssString, minifiedCSS;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4, fs_1.default.promises.readFile(cssFile, {
                                encoding: 'utf8',
                            })];
                        case 1:
                            cssString = _a.sent();
                            minifiedCSS = csso_1.default.minify(cssString).css;
                            return [4, fs_1.default.promises.writeFile(cssFile, minifiedCSS, {
                                    encoding: 'utf8',
                                })];
                        case 2:
                            _a.sent();
                            return [2];
                    }
                });
            });
        }
        var minifiers_1, htmlFiles, cssFiles;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(minify == true)) return [3, 4];
                    spinner.text = 'Minifying the static site';
                    minifiers_1 = [];
                    return [4, globby_1.default(publicDirectory + "/**/*.+(html|htm)")];
                case 1:
                    htmlFiles = _a.sent();
                    htmlFiles.forEach(function (htmlFile) {
                        minifiers_1.push(minifyHTML(htmlFile));
                    });
                    return [4, globby_1.default(publicDirectory + "/**/*.css")];
                case 2:
                    cssFiles = _a.sent();
                    cssFiles.forEach(function (cssFile) {
                        minifiers_1.push(minifyCSS(cssFile));
                    });
                    return [4, Promise.all(minifiers_1)];
                case 3:
                    _a.sent();
                    return [2];
                case 4: return [2];
            }
        });
    });
}
exports.minifyStaticSite = minifyStaticSite;
function build(args, options, config) {
    return __awaiter(this, void 0, void 0, function () {
        var publicDirectory, spinner;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    publicDirectory = path_1.default.join(process.cwd(), 'public');
                    return [4, validate_1.validateURL(args.url)];
                case 1:
                    _a.sent();
                    return [4, handlePublicDirectory(publicDirectory, options, config)];
                case 2:
                    _a.sent();
                    spinner = ora_1.default({
                        color: 'cyan',
                        hideCursor: true,
                        isEnabled: true,
                        text: 'Preparing to build static site',
                        interval: 80,
                    }).start();
                    return [4, fetchStatics(args.url, publicDirectory, spinner)];
                case 3:
                    _a.sent();
                    spinner.text = 'Fixing asset links';
                    return [4, fixWgetLinks(publicDirectory)];
                case 4:
                    _a.sent();
                    return [4, minifyStaticSite(options.minify, publicDirectory, spinner)];
                case 5:
                    _a.sent();
                    spinner.stopAndPersist({
                        symbol: chalk_1.default.greenBright('✔'),
                        text: 'Finished building static site',
                    });
                    return [2];
            }
        });
    });
}
exports.default = build;
//# sourceMappingURL=build.js.map