"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var del_1 = __importDefault(require("del"));
var inquirer_1 = __importDefault(require("inquirer"));
var js_yaml_1 = __importDefault(require("js-yaml"));
var ora_1 = __importDefault(require("ora"));
var chalk_1 = __importDefault(require("chalk"));
var config_1 = require("../config");
var errorHandler_1 = __importDefault(require("../errorHandler"));
var logger_1 = __importDefault(require("../logger"));
var validate_1 = require("../validate");
var build_1 = require("../cmd/build");
var deploy_1 = require("../cmd/deploy");
function makeConfigPath(filename) {
    return path_1.default.join(process.cwd(), filename);
}
function createNewConfig() {
    return __awaiter(this, void 0, void 0, function () {
        var alreadyExists, possiblePaths, _a, _b, _i, index, overwritePrompt, configPrompts, configFilePath, configString, configObject;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    alreadyExists = null;
                    possiblePaths = [
                        makeConfigPath('.nityarc.yml'),
                        makeConfigPath('.nityarc.json'),
                        makeConfigPath('nitya.config.js'),
                    ];
                    _a = [];
                    for (_b in possiblePaths)
                        _a.push(_b);
                    _i = 0;
                    _c.label = 1;
                case 1:
                    if (!(_i < _a.length)) return [3, 4];
                    index = _a[_i];
                    if (!(alreadyExists == null)) return [3, 3];
                    if (!fs_1.default.existsSync(possiblePaths[index])) return [3, 3];
                    return [4, inquirer_1.default.prompt({
                            type: 'confirm',
                            name: 'overwrite',
                            message: "Found a " + path_1.default.basename(possiblePaths[index]) + " file. Would you like to overwrite it?",
                            default: false,
                        })];
                case 2:
                    overwritePrompt = _c.sent();
                    if (overwritePrompt.overwrite !== true) {
                        process.exit(3);
                    }
                    _c.label = 3;
                case 3:
                    _i++;
                    return [3, 1];
                case 4: return [4, inquirer_1.default.prompt([
                        {
                            type: 'list',
                            name: 'type',
                            message: 'What type of config file should be created?',
                            default: 2,
                            choices: [
                                {
                                    title: 'YAML',
                                    value: '.nityarc.yml',
                                    description: '(.nityarc.yml) YAML Ain\'t Markup Language',
                                },
                                {
                                    title: 'JSON',
                                    value: '.nityarc.json',
                                    description: '(.nityarc.json) JavaScript object notation',
                                },
                                {
                                    title: 'JavaScript',
                                    value: 'nitya.config.js',
                                    description: '(nitya.config.js) JavaScript',
                                },
                            ],
                        },
                        {
                            type: 'input',
                            name: 'uri',
                            message: 'What is the URL of Ghost site?',
                            validate: function (value) {
                                return validate_1.validateGhostURL(value);
                            },
                        },
                        {
                            type: 'confirm',
                            message: 'Would you like to minify the generated static site?',
                            name: 'minify',
                            default: false,
                        },
                        {
                            type: 'rawlist',
                            message: 'Where should this site be deployed to?',
                            name: 'deploy',
                            choices: [{ title: 'GitHub Pages', value: 'gh-pages' }],
                        },
                        {
                            type: function (prev) {
                                return prev == 'gh-pages' ? 'text' : false;
                            },
                            message: 'What is the GitHub repository?',
                            name: 'repository',
                            validate: function (value) {
                                return validate_1.isGitURL(value);
                            },
                        },
                    ])];
                case 5:
                    configPrompts = _c.sent();
                    configFilePath = makeConfigPath(configPrompts.type);
                    configObject = {
                        minify: configPrompts.minify,
                        uri: configPrompts.uri,
                        server: config_1.defaultConfig.server,
                        deploy: {
                            provider: configPrompts.deploy,
                            repository: configPrompts.repository,
                        },
                    };
                    if (configPrompts.type == '.nityarc.yml') {
                        configString = js_yaml_1.default.safeDump(configObject, {
                            indent: 4,
                            lineWidth: 300,
                        });
                    }
                    else if (configPrompts.type == '.nityarc.json') {
                        configString = JSON.stringify(configObject, null, 4);
                    }
                    else if (configPrompts.type == 'nitya.config.js') {
                        configString = "module.exports = " + JSON.stringify(configObject, null, 4);
                    }
                    return [4, del_1.default(possiblePaths)];
                case 6:
                    _c.sent();
                    fs_1.default.writeFileSync(configFilePath, configString, {
                        encoding: 'UTF-8',
                    });
                    logger_1.default.success("A " + path_1.default.basename(configFilePath) + " is written to: " + process.cwd());
                    return [2];
            }
        });
    });
}
function buildSite(options, config) {
    return __awaiter(this, void 0, void 0, function () {
        var publicDirectory, spinner;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    publicDirectory = path_1.default.join(process.cwd(), 'public');
                    return [4, validate_1.validateURL(config.uri)];
                case 1:
                    _a.sent();
                    return [4, build_1.handlePublicDirectory(publicDirectory, options, config)];
                case 2:
                    _a.sent();
                    return [4, validate_1.checkIfDeployable(config)];
                case 3:
                    _a.sent();
                    spinner = ora_1.default({
                        color: 'cyan',
                        hideCursor: true,
                        isEnabled: true,
                        text: 'Preparing to build static site',
                        interval: 80,
                    }).start();
                    return [4, build_1.fetchStatics(config.uri, publicDirectory, spinner)];
                case 4:
                    _a.sent();
                    spinner.text = 'Fixing asset links';
                    return [4, build_1.fixWgetLinks(publicDirectory)];
                case 5:
                    _a.sent();
                    return [4, build_1.minifyStaticSite(config.minify, publicDirectory, spinner)];
                case 6:
                    _a.sent();
                    return [4, deploy_1.writeCNAME(publicDirectory, options, config)];
                case 7:
                    _a.sent();
                    return [4, deploy_1.deployStatic(publicDirectory, options, config, spinner)];
                case 8:
                    _a.sent();
                    spinner.stopAndPersist({
                        symbol: chalk_1.default.greenBright('✔'),
                        text: 'Finished building static site',
                    });
                    return [2];
            }
        });
    });
}
function root(options, config) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(options.init === true)) return [3, 1];
                    errorHandler_1.default(createNewConfig()).then(function (data) {
                        if (data.error) {
                            logger_1.default.error(data.error, 3);
                        }
                        else {
                            process.exit(0);
                        }
                    });
                    return [3, 6];
                case 1:
                    if (!(typeof config.file == 'string')) return [3, 5];
                    if (!(typeof config.uri == 'string')) return [3, 3];
                    return [4, buildSite(options, config)];
                case 2: return [2, _a.sent()];
                case 3:
                    logger_1.default.error('No source "uri" specified.', 1);
                    _a.label = 4;
                case 4: return [3, 6];
                case 5:
                    logger_1.default.error('No automatic configuration found.', 2);
                    _a.label = 6;
                case 6: return [2];
            }
        });
    });
}
exports.default = root;
//# sourceMappingURL=root.js.map