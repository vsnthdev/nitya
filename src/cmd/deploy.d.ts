import ora from 'ora';
import { ConfigImpl } from '../config';
export declare function writeCNAME(publicDirectory: any, options: any, config: ConfigImpl): Promise<void>;
export declare function deployStatic(publicDirectory: string, options: any, config: ConfigImpl, spinner: ora.Ora): Promise<void>;
export default function deploy(options: any, config: ConfigImpl): Promise<void>;
//# sourceMappingURL=deploy.d.ts.map