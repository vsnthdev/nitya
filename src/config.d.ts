import { GitHubConfigImpl } from '../lib/deployments/github';
export interface DeployProviderConfigImpl {
    provider: string;
    domain?: string;
}
export declare const possibleDeploymentProviders: string[];
export interface ConfigImpl {
    uri?: string;
    file?: string;
    minify: boolean;
    server: {
        host: string;
        port: number;
    };
    deploy?: GitHubConfigImpl;
}
export declare const defaultConfig: ConfigImpl;
export default function loadConfig(): Promise<ConfigImpl>;
export declare function getDomain(options: any, config: ConfigImpl): string;
//# sourceMappingURL=config.d.ts.map