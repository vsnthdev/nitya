#!/usr/bin/env node
//   ___    __________   |  Vasanth Developer (Vasanth Srivatsa)
//   __ |  / /___  __ \  |  ------------------------------------------------
//   __ | / / __  / / /  |  https://github.com/vasanthdeveloper/nitya.git
//   __ |/ /  _  /_/ /   |
//   _____/   /_____/    |  Entryfile for nitya cli application
//                       |

import caporal from 'caporal'
import updateNotifier from 'update-notifier'

import errorHandler from './errorHandler'
import config, { ConfigImpl, getDomain } from './config'
import logger from './logger'

import rootCmd from './cmd/root'
import buildCmd from './cmd/build'
import serveCmd from './cmd/serve'
import deployCmd from './cmd/deploy'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageInfo = require('../package.json')

// Notify users that there is a new version of this command
updateNotifier({ pkg: packageInfo, updateCheckInterval: 0 }).notify()

// 1. Load the configuration file if found
errorHandler(config()).then(data => {
    if (data.error) {
        logger.error(data.error)
    } else {
        // 2. Cast the config from data
        const config = data.data as ConfigImpl

        // 2. Set version number and description
        caporal
            .version(`v${packageInfo.version}`)
            .description(packageInfo.description)
            .option(
                '--init',
                'Creates a new configuration file in the current directory',
                caporal.BOOLEAN,
                false,
                false,
            )
            .action((args, options) => {
                rootCmd(options, config)
            })

        // 3. Define all the command line arguments
        // The build command that converts Ghost into a static site
        caporal
            .command('build', 'Convert Ghost site into static website')
            .argument(
                '[url]',
                'The Ghost site URL from which static website should be generated',
                '',
                config.uri,
            )
            .option(
                '--minify <value>',
                'Minify the static site',
                caporal.BOOLEAN,
                config.minify,
                false,
            )
            .option(
                '--force',
                'Overwrite the existing public directory. If exists',
                caporal.BOOLEAN,
                false,
                false,
            )
            .action((args, options) => {
                buildCmd(args, options, config)
            })

        // The serve command that starts an HTTP server
        caporal
            .command('serve', 'Preview the built static website')
            .option(
                '--port [number]',
                'Port number to bind',
                caporal.INTEGER,
                config.server.port,
                false,
            )
            .option(
                '--host [number]',
                'Host to listen for incoming requests',
                caporal.STRING,
                config.server.host,
                false,
            )
            .option(
                '--open',
                'Open browser after starting the server',
                caporal.BOOLEAN,
                false,
                false,
            )
            .action((args, options) => {
                serveCmd(args, options)
            })

        // The deploy command that deploys the generated static site
        caporal
            .command('deploy', 'Deploy the static site to a cloud service')
            .option(
                '--domain [domain]',
                'Creates a CNAME file while deploying for custom domains',
                caporal.STRING,
                getDomain({}, config),
                false,
            )
            .action((args, options) => {
                deployCmd(options, config)
            })

        // 4. Parse command line arguments
        caporal.parse(process.argv)
    }
})
