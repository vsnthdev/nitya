/* eslint-disable @typescript-eslint/no-explicit-any */
// This file handles errors neatly when using async/await

interface HandlerResponseImpl {
    error: Error | undefined
    data: any
}

export default function errorHandler(
    target: any,
): Promise<HandlerResponseImpl> {
    return new Promise(resolve => {
        target
            .then((anyData: any) => {
                const returnableResponse: HandlerResponseImpl = {
                    error: undefined,
                    data: anyData,
                }

                resolve(returnableResponse)
            })
            .catch((anError: any) => {
                const returnableResponse: HandlerResponseImpl = {
                    error: anError,
                    data: undefined,
                }

                resolve(returnableResponse)
            })
    })
}
