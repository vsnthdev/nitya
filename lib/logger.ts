// This file will handle log messages that are given to the console
import itivrutaha from 'itivrutaha'

export default itivrutaha.createNewLogger({
    theme: ':type :message',
})
