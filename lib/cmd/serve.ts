// This file checks if there is a public directory, if yes
// then starts a webserver and serves those up

import fs from 'fs'
import path from 'path'

import express from 'express'
import open from 'open'

import errorHandler from '../errorHandler'
import logger from '../logger'
import { ConfigImpl } from '../config'

async function startServer(options, publicDirectory: string): Promise<void> {
    // Create an express server
    const server = express()

    // Configure the static site generator for server
    server.use(express.static(publicDirectory))

    // Start the server
    server.listen(options.port, () => {
        logger.info(`Started serving on port ${options.port}`)
    })

    // Open the internet browser if open option was true
    if (options.open == true) {
        open(`${options.host}:${options.port}`)
    }
}

export async function checkIfBuilt(publicDirectory: string): Promise<void> {
    const exists = await errorHandler(fs.promises.access(publicDirectory))

    if (typeof exists.error == 'undefined') {
        return
    } else {
        logger.error('No public directory found.', 5)
    }
}

export default async function serve(args, options): Promise<void> {
    // 1. Check if a public directory exists
    const publicDirectory: string = path.join(process.cwd(), 'public')

    // 2. Check if static site was already built
    await checkIfBuilt(publicDirectory)

    // 3. Start the static site web server
    return await startServer(options, publicDirectory)
}
