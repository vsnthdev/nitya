// This is the file that converts a Ghost site into a static website

import fs from 'fs'
import path from 'path'

import validURL from 'url-validation'
import inquirer from 'inquirer'
import execa from 'execa'
import moment from 'moment'
import del from 'del'
import mkdirp from 'mkdirp'
import chalk from 'chalk'
import ora from 'ora'
import glob from 'globby'
import htmlMinifier from 'html-minifier'
import cssMinifier from 'csso'
import replaceInFile, { ReplaceInFileConfig } from 'replace-in-file'

import { ConfigImpl } from '../config'
import errorHandler from '../errorHandler'
import logger from '../logger'
import { validateURL } from '../validate'

// fetchStatics is the function that internally calls wget tool to convert a Ghost site
// into a static site
export async function fetchStatics(
    url: string,
    publicDirectory: string,
    spinner: ora.Ora,
): Promise<void> {
    // 1. Create a public directory
    mkdirp.sync(publicDirectory)

    // 2. Go into the created public directory
    process.chdir(publicDirectory)

    // 3. Prepare the variables necessary for building
    const date: string = moment().format('YYYY-MM-DD')
    const domain: string = new URL(url).host

    // 4. Start an instance of wget
    const wget = execa('/usr/bin/wget', [
        '--recursive',
        '--page-requisites',
        '--html-extension',
        '--convert-links',
        '--restrict-file-names=unix',
        '--no-parent=on',
        '--no-host-directories',
        '--no-cache',
        '--no-dns-cache',
        `--domains=${domain}`,
        url,
    ])

    // Show progress to the user
    wget.stderr.setEncoding('utf8')
    wget.stderr.on('data', (data: string) => {
        if (data.startsWith(date)) {
            const filename: string = data.split('‘')[1].split('’')[0]
            spinner.text = `Downloading ${chalk.gray(filename.split('?')[0])}`
        }
    })

    // 5. Resolve this promise when wget exists
    await wget
}

// handlePublicDirectory will handle what to do if a public directory already exists
export async function handlePublicDirectory(
    publicDirectory: string,
    options,
    config: ConfigImpl,
): Promise<void> {
    // 1. Check if a public directory actually exists
    const exists = await errorHandler(fs.promises.access(publicDirectory))

    if (typeof exists.error !== 'undefined') {
        return
    } else {
        // 2. Check if loaded from a file
        if (typeof config.file == 'string' && config.file !== '') {
            // 3. Check if force is present or not
            if (options.force == true) {
                // Delete the public directory and resolve
                await del(publicDirectory)
                return
            } else {
                logger.error('A public directory already exists', 4)
            }
        } else {
            // 3. Check if force is passed or not
            if (options.force == true) {
                // Delete the public directory and resolve
                await del(publicDirectory)
                return
            } else {
                // 4. Ask the user whether he wants to overwrite the public directory or stop
                const overwrite = await inquirer.prompt({
                    type: 'confirm',
                    message:
                        'There is already a public directory. Would you like to overwrite?',
                    name: 'overwrite',
                    default: false,
                })

                if (overwrite.overwrite == false) {
                    process.exit(0)
                } else {
                    await del(publicDirectory)
                    return
                }
            }
        }
    }
}

// fixWgetLinks will replace links in the HTML files with the correct ones
// for example: we replace *.jpgpg with *.jpg and *.pngng with *.png
export async function fixWgetLinks(publicDirectory: string): Promise<void> {
    // 1. The function that takes in all the html files and does the replacement
    async function fixLinks(htmlFiles: string[]): Promise<void> {
        // 2. Prepare a replace options object
        const replaceOptions: ReplaceInFileConfig = {
            files: htmlFiles,
            from: [/jpgpg/g, /pngng/g, /jpgg/g, /pngg/g, /jpgjpg/g, /pngpng/g],
            to: ['jpg', 'png', 'jpg', 'png', 'jpg', 'png'],
        }

        // 3. Perform the replace operation
        await replaceInFile(replaceOptions)
        return
    }

    // 2. Grab all HTML files
    const matches: string[] = await glob(`${publicDirectory}/**/*.+(html|htm)`)
    await fixLinks(matches)
    return
}

// minifyStaticSite will minify the downloaded static site
export async function minifyStaticSite(
    minify: boolean,
    publicDirectory: string,
    spinner: ora.Ora,
): Promise<void> {
    // 1. The function that minifies Hyper Text Markup files
    async function minifyHTML(htmlFile: string): Promise<void> {
        // 1. Read the HTML file
        const htmlString = await fs.promises.readFile(htmlFile, {
            encoding: 'utf8',
        })

        // 2. Prepare minification options
        const minifyOptions = {
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            conservativeCollapse: true,
            continueOnParseError: true,
            minifyCSS: true,
            minifyJS: true,
            minifyURLs: true,
            removeAttributeQuotes: true,
            removeComments: true,
            removeEmptyAttributes: true,
            sortAttributes: true,
            sortClassName: true,
        }

        // 3. Minify HTML
        const minifiedHTML = htmlMinifier.minify(htmlString, minifyOptions)

        // 4. Write back the minified HTML
        await fs.promises.writeFile(htmlFile, minifiedHTML, {
            encoding: 'utf8',
        })
    }

    // 2. The function that minifies Cascading Style Sheets
    async function minifyCSS(cssFile: string): Promise<void> {
        // 1. Read the HTML file
        const cssString = await fs.promises.readFile(cssFile, {
            encoding: 'utf8',
        })

        // 2. Minify CSS
        const minifiedCSS = cssMinifier.minify(cssString).css

        // 3. Write back the minified CSS
        await fs.promises.writeFile(cssFile, minifiedCSS, {
            encoding: 'utf8',
        })
    }

    // TODO: Implement JavaScript minification using TerserJS

    // 4. Only act if minify option was set
    if (minify == true) {
        spinner.text = 'Minifying the static site'

        // The variable that stores promises to all the minifications
        const minifiers = []

        // Grab appropriate files and pass them to the above functions
        const htmlFiles: string[] = await glob(
            `${publicDirectory}/**/*.+(html|htm)`,
        )
        htmlFiles.forEach((htmlFile: string) => {
            minifiers.push(minifyHTML(htmlFile))
        })

        const cssFiles: string[] = await glob(`${publicDirectory}/**/*.css`)
        cssFiles.forEach((cssFile: string) => {
            minifiers.push(minifyCSS(cssFile))
        })

        // Once all our promises have resolved
        await Promise.all(minifiers)
        return
    } else {
        return
    }
}

export default async function build(
    args,
    options,
    config: ConfigImpl,
): Promise<void> {
    // 1. Prepare all the required variables
    const publicDirectory: string = path.join(process.cwd(), 'public')

    // 2. Do all sorts of checks before we commit to building
    await validateURL(args.url)
    await handlePublicDirectory(publicDirectory, options, config)

    // 6. Prepare a ora spinner
    const spinner = ora({
        color: 'cyan',
        hideCursor: true,
        isEnabled: true,
        text: 'Preparing to build static site',
        interval: 80,
    }).start()

    await fetchStatics(args.url, publicDirectory, spinner)
    spinner.text = 'Fixing asset links'
    await fixWgetLinks(publicDirectory)
    await minifyStaticSite(options.minify, publicDirectory, spinner)

    spinner.stopAndPersist({
        symbol: chalk.greenBright('✔'),
        text: 'Finished building static site',
    })
}
