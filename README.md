<h1 align="center"><a href="https://nitya.vasanthdeveloper.com">
    <img src="https://raw.githubusercontent.com/vasanthdeveloper/nitya/media/PNG/github_logo.png" alt="nitya">
</a></h1>
<p align="center">
    <a href="https://npmjs.com/package/nitya">
        <img src="https://img.shields.io/npm/v/nitya.svg?style=flat-square" alt="npmjs">
    </a>
    <a href="https://github.com/vasanthdeveloper/nitya/releases">
        <img src="https://img.shields.io/npm/dm/nitya.svg?style=flat-square" alt="releases">
    </a>
    <a href="https://github.com/vasanthdeveloper/nitya/issues">
        <img src="https://img.shields.io/github/issues/vasanthdeveloper/nitya.svg?style=flat-square" alt="issues">
    </a>
    <a href="https://github.com/vasanthdeveloper/nitya/commits/mainline">
        <img src="https://img.shields.io/github/last-commit/vasanthdeveloper/nitya.svg?style=flat-square"
            alt="commits">
    </a>
</p>
<p align="center">
    <a href="https://nitya.vasanthdeveloper.com">Docs</a> •
    <a href="#-installation">Installation</a> •
    <a href="#-quick-start">Quick Start</a> •
    <a href="#-contributions--issues">Support</a> •
    <a href="LICENSE.md">License</a>
</p>

<!-- TODO: Add link to static site which points to an article mentioning "benefits of static sites" -->
`nitya` is a static site generator that converts a <a target="_blank" rel="noopener" href="https://ghost.org">Ghost</a>  site into a static site using <a target="_blank" rel="noopener" href="https://en.wikipedia.org/wiki/Web_scraping">web scraping</a> and <a target="_blank" rel="noopener" href="https://www.gnu.org/software/wget"><code>wget</code></a>. The project was inspired by <a target="_blank" ref="noopener" href="https://github.com/axitkhurana/buster">buster</a>, which was written in Python 2 and is no longer being maintained. `nitya` aims to integrate into existing JavaScript projects while providing a completely automated generation and deployment process and great support for execution inside a CI/CD pipeline.

> Tweet me [@vasanthdevelope](https://vasanth.tech/twitter) and share your opinion and experience with this project 😃

# 💿 Installation
[![NPM](https://nodei.co/npm/nitya.png)](https://nodei.co/npm/nitya/)
```
npm install -g nitya
```
Or, if you are using Yarn, then
```
yarn global add nitya
```
> **Note:** Installing on Linux or macOS requires to be run with `sudo`.

# 🚀 Quick Start
After installing `nitya` using the above command(s), you can use it in two ways:
#### 🤖 Automatic _(Recommended)_
For `nitya` to run automatically without any further user interaction, it requires a configuration file in the working directory. The config file can be a YAML, JSON or even a JavaScript file that tells `nitya` where the Ghost site is hosted, any processing to be done, and where to deploy the processed static site. To create a new config file in the current working directory run the command:
```
nitya --init
```
To start `nitya` in automated mode, simply run the command:
```
nitya
```
#### 🧗 Manual
The manual mode gives more control over the static site that is being built. It is useful if the deployment provider you intend to use isn't supported by `nitya` yet.
```
# To convert a Ghost site into a static site, run the command:
nitya build <URL of the Ghost site>

To preview the built static site, run the command:
nitya serve
```
As of now, to leverage the deployment process of `nitya` a configuration file is required. Although, you can implement your own deployment process.

# 📹 Demo
All my Ghost theme projects ([aakari](https://aakari.vasanthdeveloper.com), [samskara](https://samskara.vasanthdeveloper.com) and kevala)'s previews are generated using `nitya`.

# ✔️ Todo
- [ ] Support for Netlify deployment
- [ ] Ability to deploy without creating a config file
- [ ] Better logging for CI/CD systems where `tty` won't be interactive
- [ ] Enable support for GZipped servers
- [ ] Implement JavaScript minification using TerserJS

# 🔬 Contributions & Issues
All _pull requests_ are welcome! If you would like to discuss more about the project, you can join my Discord server or message me directly via Telegram/Twitter.

# 🎉 Support
I work 24x7 on projects like this, and make educational computer videos. Although I don't do this for making money. Money is required, for me to live and eat as well as buy equipment and continue to maintain projects like this. If you have a heart 💖 and value my efforts. Please donate me a small amount through PayPal or become my valued patron over at Patreon.

<a target="_blank" rel="noopener" href="https://vasanth.tech/patreon"><img height="32px" src="https://github.com/vasanthdeveloper/nitya/blob/media/PNG/patreon_button.png?raw=true"></a>
<a target="_blank" rel="noopener" href="https://vasanth.tech/paypal"><img height="32px" src="https://github.com/vasanthdeveloper/nitya/blob/media/PNG/paypal_button.png?raw=true"></a>

# 📜 License
> _The nitya project is released under the [MIT license](LICENSE.md). <br> Developed &amp; maintained By Vasanth Developer. Copyright 2020 © Vasanth Developer._
<hr>

> [vasanth.tech](https://vasanth.tech) &nbsp;&middot;&nbsp;
> YouTube [@vasanthdeveloper](https://vasanth.tech/youtube) &nbsp;&middot;&nbsp;
> Twitter [@vasanthdevelope](https://vasanth.tech/twitter) &nbsp;&middot;&nbsp;
> Discord [Vasanth Developer](https://vasanth.tech/discord)